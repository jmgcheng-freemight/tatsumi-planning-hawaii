jQuery(document).ready(function($){

  $('#my_theme_option').cookieTab({
   tabMenuElm: '#theme_tab',
   tabPanelElm: '#tab-panel'
  });


  // fancybox
  $(".fancybox").fancybox();

	// radio button for page custom fields

   $(".ml_custom_fields .template li label").click(function () {
     $(".ml_custom_fields .template li label").removeClass('active');
     $(this).addClass('active');
   });

   $("#template_type1").click(function () {
    $("#ml_custom_fields_box2").hide();
    $("#ml_custom_fields_box3").hide();
    $("#ml_custom_fields_box4").hide();
    $("#ml_custom_fields_box5").hide();
   });

   $("#template_type2").click(function () {
    $("#ml_custom_fields_box2").show();
    $("#ml_custom_fields_box3").hide();
    $("#ml_custom_fields_box4").hide();
    $("#ml_custom_fields_box5").hide();
   });

   $("#template_type3").click(function () {
    $("#ml_custom_fields_box3").show();
    $("#ml_custom_fields_box2").hide();
    $("#ml_custom_fields_box4").hide();
    $("#ml_custom_fields_box5").hide();
   });

   $("#template_type4").click(function () {
    $("#ml_custom_fields_box4").show();
    $("#ml_custom_fields_box2").hide();
    $("#ml_custom_fields_box3").hide();
    $("#ml_custom_fields_box5").hide();
   });

   $("#template_type5").click(function () {
    $("#ml_custom_fields_box5").show();
    $("#ml_custom_fields_box2").hide();
    $("#ml_custom_fields_box3").hide();
    $("#ml_custom_fields_box4").hide();
   });

});
<?php

define('CHILD_TEMPLATE_URI', dirname( get_bloginfo('stylesheet_url')) );


// テーマオプション --------------------------------------------------------------------------------
require_once ( dirname(__FILE__) . '/admin/theme-options.php' );



function widget_admin_scripts_child() {
	wp_enqueue_script('my_script_child', CHILD_TEMPLATE_URI.'/admin/js/my_script_child.js');
}
add_action('admin_print_scripts', 'widget_admin_scripts_child');



function scripts_child() {
	wp_enqueue_script( 'jscript_child', CHILD_TEMPLATE_URI.'/js/jscript_child.js', array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'scripts_child' );







// ページ用カスタムフィールド --------------------------------------------------------------------------------
require dirname(__FILE__) . '/functions/page_cf.php';



remove_filter( 'the_content', 'wpautop' );
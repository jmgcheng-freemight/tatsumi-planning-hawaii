jQuery(document).ready(function($){

  try {

    function initGmap() {
      /**/
      var o_mapProp_1 = {
        center      :new google.maps.LatLng(21.301065, -157.862179),
        zoom        :17,
        mapTypeId   :google.maps.MapTypeId.ROADMAP
      };
      var o_map_1 = new google.maps.Map(document.getElementById("js-maphawaii"),o_mapProp_1);
      var o_marker_1 = new google.maps.Marker({
        position    :new google.maps.LatLng(21.301065, -157.862179),
      });
      o_marker_1.setMap(o_map_1);

      /**/
      var o_mapProp_2 = {
        center      :new google.maps.LatLng(35.4570517,139.6340909),
        zoom        :18,
        mapTypeId   :google.maps.MapTypeId.ROADMAP
      };
      var o_map_2 = new google.maps.Map(document.getElementById("js-japan"),o_mapProp_2);
      var o_marker_2 = new google.maps.Marker({
        position    :new google.maps.LatLng(35.4570517,139.6340909),
      });
      o_marker_2.setMap(o_map_2);


    }
    google.maps.event.addDomListener(window, 'load', initGmap);   
  }
  catch (e) {}

  try {
    $( "#main-menu.js-main-menu-sp" ).append( '<li class="menu-item menu-item-type-custom open"><a href="contact">お問い合わせ</a></li>' );
  }
  catch (e) {}


});
<?php /* Template Name: Welcome Screen */
get_header();
$options = get_desing_plus_option();
?>

  <?php $slider_image1 = wp_get_attachment_image_src($options['slider_image1'], 'full'); ?>
  <?php $slider_image2 = wp_get_attachment_image_src($options['slider_image2'], 'full'); ?>
  <?php $slider_image3 = wp_get_attachment_image_src($options['slider_image3'], 'full'); ?>
  <div id="top" class="heightasviewport">
    <?php if($options['slider_url1']){ echo '<a href="'.$options['slider_url1'].'">'; }; ?><div class="heightasviewport splash-image" style="background-image:url(<?php echo $slider_image1[0]; ?>)"></div><?php if($options['slider_url1']){ echo '</a>'; }; ?>
    <?php if($options['slider_url2']){ echo '<a href="'.$options['slider_url2'].'">'; }; ?><div class="heightasviewport splash-image" style="background-image:url(<?php echo $slider_image2[0]; ?>)"></div><?php if($options['slider_url2']){ echo '</a>'; }; ?>
    <?php if($options['slider_url3']){ echo '<a href="'.$options['slider_url3'].'">'; }; ?><div class="heightasviewport splash-image" style="background-image:url(<?php echo $slider_image3[0]; ?>)"></div><?php if($options['slider_url3']){ echo '</a>'; }; ?>

    <div class="heightasviewport no-pointer-events" style="position:relative">
      <div class="verticalcenter container">
        <div class="row">
          <div id="agent-splash-text" class="col-xs-120 translated-right serif animated">
            <h2 class="agent-splash-text-h2 smaller-mobile-h2"><?php echo nl2br($options['top_content1_headline']); ?></h2>
            
            <p class="cwhite f17">
              多くの日本人観光客が訪れるハワイ、皆さん一度は「ハワイに住みたい！」と思われたこ <br/>
              とがあるのではないでしょうか？ただ、思われただけで無理だとあきらめていませんか？ <br/>
              そんなことはありません、手続きを行いビザを取得すればそれも無理ではありません。
            </p>

          </div>
        </div>
      </div>
    </div>

    <?php if($options['top_content1_btnsize']!='0'): ?>
    <a href="#section-two" class="next-button animated serif" style="width:<?php echo $options['top_content1_btnsize']; ?>px; height:<?php echo $options['top_content1_btnsize']; ?>px;">
        <span><?php echo nl2br($options['top_content1_btnlabel']); ?></span>
    </a>
    <?php endif; ?>
  </div>

  <div class="container">
    <div class="row" style="position: relative;">
      <div class="col-xs-120">
        <p class="text-center" style="position:absolute; top: -30px; left: 50%; min-width: 280px;  -ms-transform: translate(-50%,0); -webkit-transform: translate(-50%,0); -moz-transform: translate(-50%,0); -o-transform: translate(-50%,0); transform: translate(-50%,0);">
          <a href="license_hawaii" target="_blank" class="cwhite decoration-underline">ハワイ州公認建設ライセンス (BC-34005)</a>
        </p>
      </div>
    </div>
  </div>

  <?php $top_main_image2 = wp_get_attachment_image_src($options['top_main_image2'], 'circle2'); ?>
  <?php 
    if( isset($top_main_image2) && !empty($top_main_image2) ) :
  ?>
  <div class="section" id="section-two">
    <div class="container">
      <div class="row">
        <div class="col-xs-120 text-center">
          <h3 class="section-two-h3 smaller-mobile-h2"><?php echo nl2br($options['top_content2_headline']); ?></h3>
          <div class="desc1"><?php echo nl2br($options['top_content2_bodytext']); ?></div>
          <div class="text-center">
            <img src="<?php echo $top_main_image2[0]; ?>">
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php 
    endif;
  ?>

  <?php $top_main_image3 = wp_get_attachment_image_src($options['top_main_image3'], 'full'); ?>
  <div class="separator" data-parallax="scroll" data-speed="0.6" data-image-src="<?php echo $top_main_image3[0]; ?>">
    <div class="title">
      <div class="container">
        <div class="row">
          <div class="col-xs-120">
            <h4 class="liner"><?php echo $options['top_content3_headline']; ?></h4>
            <span class="lead romaji"><?php echo $options['top_content3_headline_sub']; ?></span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php
    $top_content3_banner_image1 = wp_get_attachment_image_src($options['top_content3_banner_image1'], 'circle3');
    $top_content3_banner_image2 = wp_get_attachment_image_src($options['top_content3_banner_image2'], 'circle3');
    $top_content3_banner_image3 = wp_get_attachment_image_src($options['top_content3_banner_image3'], 'circle3');
    $top_content3_banner_image4 = wp_get_attachment_image_src($options['top_content3_banner_image4'], 'circle3');
  ?>
  <div class="section container">
    <div class="row">
      <div class="col-xs-120 no-padding content-card-list">
        <div class="row text-left">


          <div class="text-center col-sm-30 mobile-mb-60" style="padding-left:30px;padding-right:30px;">
            <?php if($options['top_content3_banner_url1']){ ?><div class="circleimages"><a href="<?php echo $options['top_content3_banner_url1']; ?>"><img src="<?php echo $top_content3_banner_image1[0]; ?>"/></a></div><?php }else{ ?><img src="<?php echo $top_content3_banner_image1[0]; ?>"/><?php }; ?>
            <h5 class="text-center section3-h5"><?php echo nl2br($options['top_content3_banner_headline1']); ?></h5>
            <div class="text-justify desc2"><?php echo nl2br($options['top_content3_banner_body1']); ?></div>
            <?php if($options['top_content3_banner_url1']){ ?><a class="read-more romaji mobile-mt-0" href="<?php echo $options['top_content3_banner_url1']; ?>"><?php echo $options['top_content3_banner_btnlabel1']; ?></a><?php }; ?>
          </div>

          <div class="text-center col-sm-30 mobile-mb-60" style="padding-left:30px;padding-right:30px;">
            <?php if($options['top_content3_banner_url2']){ ?><div class="circleimages"><a href="<?php echo $options['top_content3_banner_url2']; ?>"><img src="<?php echo $top_content3_banner_image2[0]; ?>"/></a></div><?php }else{ ?><img src="<?php echo $top_content3_banner_image2[0]; ?>" 
/><?php }; ?>
            <h5 class="text-center section3-h5"><?php echo nl2br($options['top_content3_banner_headline2']); ?></h5>
            <div class="text-justify desc2"><?php echo nl2br($options['top_content3_banner_body2']); ?></div>
            <?php if($options['top_content3_banner_url2']){ ?><a class="read-more romaji mobile-mt-0" href="<?php echo $options['top_content3_banner_url2']; ?>"><?php echo $options['top_content3_banner_btnlabel2']; ?></a><?php }; ?>
          </div>

          <div class="text-center col-sm-30 mobile-mb-60" style="padding-left:30px;padding-right:30px;">
            <?php if($options['top_content3_banner_url3']){ ?><div class="circleimages"><a href="<?php echo $options['top_content3_banner_url3']; ?>"><img src="<?php echo $top_content3_banner_image3[0]; ?>"/></a></div><?php }else{ ?><img src="<?php echo $top_content3_banner_image3[0]; ?>"/><?php }; ?>
            <h5 class="text-center section3-h5"><?php echo nl2br($options['top_content3_banner_headline3']); ?></h5>
            <div class="text-justify desc2"><?php echo nl2br($options['top_content3_banner_body3']); ?></div>
            <?php if($options['top_content3_banner_url3']){ ?><a class="read-more romaji mobile-mt-0" href="<?php echo $options['top_content3_banner_url3']; ?>"><?php echo $options['top_content3_banner_btnlabel3']; ?></a><?php }; ?>
          </div>

          <div class="text-center col-sm-30" style="padding-left:30px;padding-right:30px;">
            <?php if($options['top_content3_banner_url4']){ ?><div class="circleimages"><a href="<?php echo $options['top_content3_banner_url4']; ?>"><img src="<?php echo $top_content3_banner_image4[0]; ?>"/></a></div><?php }else{ ?><img src="<?php echo $top_content3_banner_image4[0]; ?>"/><?php }; ?>
            <h5 class="text-center section3-h5"><?php echo nl2br($options['top_content3_banner_headline4']); ?></h5>
            <div class="text-justify desc2"><?php echo nl2br($options['top_content3_banner_body4']); ?></div>
            <?php if($options['top_content3_banner_url4']){ ?><a class="read-more romaji mobile-mt-0" href="<?php echo $options['top_content3_banner_url4']; ?>"><?php echo $options['top_content3_banner_btnlabel4']; ?></a><?php }; ?>
          </div>

        </div>
      </div>
    </div>
  </div>

  <?php $top_main_image4 = wp_get_attachment_image_src($options['top_main_image4'], 'full'); ?>
  <div class="separator" data-parallax="scroll" data-speed="0.6" data-image-src="<?php echo $top_main_image4[0]; ?>">
    <div class="title">
      <div class="container">
        <div class="row">
          <div class="col-xs-120">
            <h4 class="liner"><?php echo $options['top_content4_headline']; ?></h4>
            <span class="lead romaji"><?php echo $options['top_content4_headline_sub']; ?></span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="main-content">
    <div class="section container">
      <div class="row">
          <?php $the_query = new WP_Query("post_type=post&posts_per_page=6&orderby=date&order=DESC"); ?>
          <?php if ( $the_query->have_posts() ) : ?>

            <?php /* Start the Loop */ ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <div class="col-sm-40 top_content4_post"><?php get_template_part( 'content', get_post_format() ); ?></div>
            <?php endwhile; ?>

          <?php endif; ?>
       <?php if($options['top_content4_indexurl']) { ?><a href="<?php echo $options['top_content4_indexurl']; ?>" class="archives_btn"><?php if($options['top_content4_indexlabel']){ echo $options['top_content4_indexlabel']; }else{ _e('Blog Index', 'tcd-w'); }; ?></a><?php }; ?>
    </div><!-- close .row -->
  </div><!-- close .container -->
</div><!-- close .main-content -->


<?php get_footer(); ?>
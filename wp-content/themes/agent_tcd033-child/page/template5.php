<?php
  if(get_post_meta($post->ID,'page_main_image',true)) {
  if(is_mobile()){
    if(get_post_meta($post->ID, 'page_main_image_mobile', true)){
      $value = get_post_meta($post->ID, 'page_main_image_mobile', true);
    }else{
      $value = get_post_meta($post->ID, 'page_main_image', true);
    }
  }else{
    $value = get_post_meta($post->ID, 'page_main_image', true);
  };
  }else{
  if(has_post_thumbnail()){
    $value = get_post_thumbnail_id($post->ID);
  };
  };
  $image = wp_get_attachment_image_src($value, 'full');
?>


<?php 
  /**/
  $type5_pt1headline1 = get_post_meta($post->ID,'type5_pt1headline1',true);
  $type5_pt1headline1_fontsize = get_post_meta($post->ID,'type5_pt1headline1_fontsize',true);
  /**/
  $type5_pt1headline2 = get_post_meta($post->ID,'type5_pt1headline2',true);
  $type5_pt1headline2_fontsize = get_post_meta($post->ID,'type5_pt1headline2_fontsize',true);
  /**/
  $type5_pt1desc1 = get_post_meta($post->ID,'type5_pt1desc1',true);
  /**/
  $type5_pt2featimage = get_post_meta($post->ID, 'type5_pt2featimage', true);
  if(!empty($type5_pt2featimage)) { $o_type5_pt2featimage = wp_get_attachment_image_src($type5_pt2featimage, 'full'); };
  /**/
  $type5_pt2featheadline1 = get_post_meta($post->ID,'type5_pt2featheadline1',true);
  /**/
  $type5_pt2featdesc1 = get_post_meta($post->ID,'type5_pt2featdesc1',true);




  /**/
  $type5_pt2cardlist1cardimage1 = get_post_meta($post->ID, 'type5_pt2cardlist1cardimage1', true);
  if(!empty($type5_pt2cardlist1cardimage1)) { $o_type5_pt2cardlist1cardimage1 = wp_get_attachment_image_src($type5_pt2cardlist1cardimage1, 'full'); };
  /**/
  $type5_pt2cardlist1cardheadline1 = get_post_meta($post->ID,'type5_pt2cardlist1cardheadline1',true);
  /**/
  $type5_pt2cardlist1carddesc1 = get_post_meta($post->ID,'type5_pt2cardlist1carddesc1',true);
  /**/
  $type5_pt2cardlist1cardimage2 = get_post_meta($post->ID, 'type5_pt2cardlist1cardimage2', true);
  if(!empty($type5_pt2cardlist1cardimage2)) { $o_type5_pt2cardlist1cardimage2 = wp_get_attachment_image_src($type5_pt2cardlist1cardimage2, 'full'); };
  /**/
  $type5_pt2cardlist1cardheadline2 = get_post_meta($post->ID,'type5_pt2cardlist1cardheadline2',true);
  /**/
  $type5_pt2cardlist1carddesc2 = get_post_meta($post->ID,'type5_pt2cardlist1carddesc2',true);
  /**/
  $type5_pt2cardlist1cardimage3 = get_post_meta($post->ID, 'type5_pt2cardlist1cardimage3', true);
  if(!empty($type5_pt2cardlist1cardimage3)) { $o_type5_pt2cardlist1cardimage3 = wp_get_attachment_image_src($type5_pt2cardlist1cardimage3, 'full'); };
  /**/
  $type5_pt2cardlist1cardheadline3 = get_post_meta($post->ID,'type5_pt2cardlist1cardheadline3',true);
  /**/
  $type5_pt2cardlist1carddesc3 = get_post_meta($post->ID,'type5_pt2cardlist1carddesc3',true);
  /**/
  $type5_pt2cardlist1cardimage4 = get_post_meta($post->ID, 'type5_pt2cardlist1cardimage4', true);
  if(!empty($type5_pt2cardlist1cardimage4)) { $o_type5_pt2cardlist1cardimage4 = wp_get_attachment_image_src($type5_pt2cardlist1cardimage4, 'full'); };
  /**/
  $type5_pt2cardlist1cardheadline4 = get_post_meta($post->ID,'type5_pt2cardlist1cardheadline4',true);
  /**/
  $type5_pt2cardlist1carddesc4 = get_post_meta($post->ID,'type5_pt2cardlist1carddesc4',true);
  /**/
  $type5_pt2cardlist1cardimage5 = get_post_meta($post->ID, 'type5_pt2cardlist1cardimage5', true);
  if(!empty($type5_pt2cardlist1cardimage5)) { $o_type5_pt2cardlist1cardimage5 = wp_get_attachment_image_src($type5_pt2cardlist1cardimage5, 'full'); };
  /**/
  $type5_pt2cardlist1cardheadline5 = get_post_meta($post->ID,'type5_pt2cardlist1cardheadline5',true);
  /**/
  $type5_pt2cardlist1carddesc5 = get_post_meta($post->ID,'type5_pt2cardlist1carddesc5',true);


  /**/
  $type5_pt2concernlist1headline1 = get_post_meta($post->ID,'type5_pt2concernlist1headline1',true);
  /**/
  $type5_pt2concernlist1concern1 = get_post_meta($post->ID,'type5_pt2concernlist1concern1',true);
  /**/
  $type5_pt2concernlist1concern2 = get_post_meta($post->ID,'type5_pt2concernlist1concern2',true);
  /**/
  $type5_pt2concernlist1concern3 = get_post_meta($post->ID,'type5_pt2concernlist1concern3',true);
  /**/
  $type5_pt2concernlist1concern4 = get_post_meta($post->ID,'type5_pt2concernlist1concern4',true);
  /**/
  $type5_pt2concernlist1concern5 = get_post_meta($post->ID,'type5_pt2concernlist1concern5',true);
  /**/
  $type5_pt2concernlist1desc1 = get_post_meta($post->ID,'type5_pt2concernlist1desc1',true);
  /**/
  $type5_pt2concernlist1desc2 = get_post_meta($post->ID,'type5_pt2concernlist1desc2',true);
  /**/
  $type5_pt2concernlist1desc3 = get_post_meta($post->ID,'type5_pt2concernlist1desc3',true);
  /**/
  $type5_pt2concernlist1desc4 = get_post_meta($post->ID,'type5_pt2concernlist1desc4',true);
  /**/
  $type5_pt2concernlist1desc5 = get_post_meta($post->ID,'type5_pt2concernlist1desc5',true);


  /**/
  $type5_pt2cardlist2desc1 = get_post_meta($post->ID,'type5_pt2cardlist2desc1',true);
  /**/
  $type5_pt2cardlist2cardimage1 = get_post_meta($post->ID, 'type5_pt2cardlist2cardimage1', true);
  if(!empty($type5_pt2cardlist2cardimage1)) { $o_type5_pt2cardlist2cardimage1 = wp_get_attachment_image_src($type5_pt2cardlist2cardimage1, 'full'); };
  /**/
  $type5_pt2cardlist2cardheadline1 = get_post_meta($post->ID,'type5_pt2cardlist2cardheadline1',true);
  /**/
  $type5_pt2cardlist2carddesc1 = get_post_meta($post->ID,'type5_pt2cardlist2carddesc1',true);
  /**/
  $type5_pt2cardlist2cardimage2 = get_post_meta($post->ID, 'type5_pt2cardlist2cardimage2', true);
  if(!empty($type5_pt2cardlist2cardimage2)) { $o_type5_pt2cardlist2cardimage2 = wp_get_attachment_image_src($type5_pt2cardlist2cardimage2, 'full'); };
  /**/
  $type5_pt2cardlist2cardheadline2 = get_post_meta($post->ID,'type5_pt2cardlist2cardheadline2',true);
  /**/
  $type5_pt2cardlist2carddesc2 = get_post_meta($post->ID,'type5_pt2cardlist2carddesc2',true);


  /**/
  $type5_pt2desclist1headline1 = get_post_meta($post->ID,'type5_pt2desclist1headline1',true);
  /**/
  $type5_pt2desclist1descheadline1 = get_post_meta($post->ID,'type5_pt2desclist1descheadline1',true);
  /**/
  $type5_pt2desclist1descdescription1 = get_post_meta($post->ID,'type5_pt2desclist1descdescription1',true);
  /**/
  $type5_pt2desclist1descheadline2 = get_post_meta($post->ID,'type5_pt2desclist1descheadline2',true);
  /**/
  $type5_pt2desclist1descdescription2 = get_post_meta($post->ID,'type5_pt2desclist1descdescription2',true);
  /**/
  $type5_pt2desclist1descheadline3 = get_post_meta($post->ID,'type5_pt2desclist1descheadline3',true);
  /**/
  $type5_pt2desclist1descdescription3 = get_post_meta($post->ID,'type5_pt2desclist1descdescription3',true);
  /**/
  $type5_pt2desclist1descheadline4 = get_post_meta($post->ID,'type5_pt2desclist1descheadline4',true);
  /**/
  $type5_pt2desclist1descdescription4 = get_post_meta($post->ID,'type5_pt2desclist1descdescription4',true);
  /**/
  $type5_pt2desclist1description1 = get_post_meta($post->ID,'type5_pt2desclist1description1',true);
?>


<?php if($value): ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-xs-120 page-splash hidden-xs" data-parallax="scroll" data-image-src="<?php echo $image[0]; ?>"></div>
    <div class="col-xs-120 visible-xs" style="padding:0; height:300px; background-image:url(<?php echo $image[0]; ?>); background-size:cover; background-repeat: no-repeat; background-position:center center;"></div>
  </div>
</div>
<?php endif; ?>

<div class="container" style="background: white">
  <div class="row">
    <div class="col-xs-120">
      <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
          <?php 
          if(function_exists('bcn_display'))
          {
              bcn_display();
          }
          ?>
      </div>
    </div>
  </div>
</div>

<section class="container lh220pr" style="<?php if(!$value){ echo 'margin-top:215px;'; } ?> background:white">

  <?php 
    if(     (isset($type5_pt1headline1) && !empty($type5_pt1headline1)) 
        ||  (isset($type5_pt1headline2) && !empty($type5_pt1headline2)) 
        ||  (isset($type5_pt1desc1) && !empty($type5_pt1desc1)) 
    ) :
  ?>
        <div class="row">
          <div class="col-sm-120">
            <?php 
              if( isset($type5_pt1headline1) && !empty($type5_pt1headline1) ) :
            ?>
            <h1 class="headline text-center " <?php if( isset($type5_pt1headline1_fontsize) && !empty($type5_pt1headline1_fontsize) ) { echo 'style="font-size: ' . $type5_pt1headline1_fontsize . 'px; "'; } ?> >
              <?php echo $type5_pt1headline1; ?>
            </h1>
            <?php 
              endif;
            ?>

            <?php 
              if( isset($type5_pt1headline2) && !empty($type5_pt1headline2) ) :
            ?>
            <h2 class="headline text-center " <?php if( isset($type5_pt1headline2_fontsize) && !empty($type5_pt1headline2_fontsize) ) { echo 'style="font-size: ' . $type5_pt1headline2_fontsize . 'px; "'; } ?> >
              <?php echo $type5_pt1headline2; ?>
            </h2>
            <?php 
              endif;
            ?>

            <?php 
              if( isset($type5_pt1desc1) && !empty($type5_pt1desc1) ) :
            ?>
            <div class="desc1 page_desc">
              <?php echo $type5_pt1desc1; ?>
            </div>
            <?php 
              endif;
            ?>
          </div>
        </div>
  <?php 
    endif ;
  ?>

</section>





  <?php 
    /*if(     (isset($o_type5_pt2featimage) && !empty($o_type5_pt2featimage)) 
        ||  (isset($type5_pt2featheadline1) && !empty($type5_pt2featheadline1)) 
        ||  (isset($type5_pt2featdesc1) && !empty($type5_pt2featdesc1)) 
    ) :*/
  ?>
    <section class="lh220pr">
      <div class="mb80 separator" <?php if( isset($o_type5_pt2featimage[0]) && !empty($o_type5_pt2featimage[0]) ) { echo 'data-parallax="scroll" data-speed="0.6" data-image-src="'. $o_type5_pt2featimage[0] .'"'; } ?> >
        <div class="title">
          <div class="container">
            <div class="row">
              <div class="col-xs-120">
                <?php 
                  if( isset($type5_pt2featheadline1) && !empty($type5_pt2featheadline1) ) :
                ?>
                <h2 class="liner">
                  <?php echo $type5_pt2featheadline1; ?>
                </h2>
                <?php 
                  endif;
                ?>
                <?php 
                  if( isset($type5_pt2featdesc1) && !empty($type5_pt2featdesc1) ) :
                ?>
                <span class="lead romaji">
                  <?php echo $type5_pt2featdesc1; ?>
                </span>
                <?php 
                  endif;
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row mb40">
          <div class="col-xs-120 col-sm-24">
            <div class=" resmigrate-card-item border-gray p10 ">
              <?php 
                if( isset($o_type5_pt2cardlist1cardimage1[0]) && !empty($o_type5_pt2cardlist1cardimage1[0]) ) :
              ?>
              <img class="image text-center" src="<?php echo $o_type5_pt2cardlist1cardimage1[0]; ?>" />
              <?php 
                endif;
              ?>
              <?php 
                if( isset($type5_pt2cardlist1cardheadline1) && !empty($type5_pt2cardlist1cardheadline1) ) :
              ?>
              <h3 class="text-center headline">
                <?php echo $type5_pt2cardlist1cardheadline1; ?>
              </h3>
              <?php 
                endif ;
              ?>
              <?php 
                if( isset($type5_pt2cardlist1carddesc1) && !empty($type5_pt2cardlist1carddesc1) ) :
              ?>
              <p class="description">
                <?php echo $type5_pt2cardlist1carddesc1; ?>
              </p>
              <?php 
                endif ;
              ?>
            </div>
          </div>
          <div class="col-xs-120 col-sm-24">
            <div class=" resmigrate-card-item border-gray p10 ">
              <?php 
                if( isset($o_type5_pt2cardlist1cardimage2[0]) && !empty($o_type5_pt2cardlist1cardimage2[0]) ) :
              ?>
              <img class="image text-center" src="<?php echo $o_type5_pt2cardlist1cardimage2[0]; ?>" />
              <?php 
                endif;
              ?>
              <?php 
                if( isset($type5_pt2cardlist1cardheadline2) && !empty($type5_pt2cardlist1cardheadline2) ) :
              ?>
              <h3 class="text-center headline">
                <?php echo $type5_pt2cardlist1cardheadline2; ?>
              </h3>
              <?php 
                endif ;
              ?>
              <?php 
                if( isset($type5_pt2cardlist1carddesc2) && !empty($type5_pt2cardlist1carddesc2) ) :
              ?>
              <p class="description">
                <?php echo $type5_pt2cardlist1carddesc2; ?>
              </p>
              <?php 
                endif ;
              ?>
            </div>
          </div>
          <div class="col-xs-120 col-sm-24">
            <div class=" resmigrate-card-item border-gray p10 ">
              <?php 
                if( isset($o_type5_pt2cardlist1cardimage3[0]) && !empty($o_type5_pt2cardlist1cardimage3[0]) ) :
              ?>
              <img class="image text-center" src="<?php echo $o_type5_pt2cardlist1cardimage3[0]; ?>" />
              <?php 
                endif;
              ?>
              <?php 
                if( isset($type5_pt2cardlist1cardheadline3) && !empty($type5_pt2cardlist1cardheadline3) ) :
              ?>
              <h3 class="text-center headline">
                <?php echo $type5_pt2cardlist1cardheadline3; ?>
              </h3>
              <?php 
                endif ;
              ?>
              <?php 
                if( isset($type5_pt2cardlist1carddesc3) && !empty($type5_pt2cardlist1carddesc3) ) :
              ?>
              <p class="description">
                <?php echo $type5_pt2cardlist1carddesc3; ?>
              </p>
              <?php 
                endif ;
              ?>
            </div>
          </div>
          <div class="col-xs-120 col-sm-24">
            <div class=" resmigrate-card-item border-gray p10 ">
              <?php 
                if( isset($o_type5_pt2cardlist1cardimage4[0]) && !empty($o_type5_pt2cardlist1cardimage4[0]) ) :
              ?>
              <img class="image text-center" src="<?php echo $o_type5_pt2cardlist1cardimage4[0]; ?>" />
              <?php 
                endif;
              ?>
              <?php 
                if( isset($type5_pt2cardlist1cardheadline4) && !empty($type5_pt2cardlist1cardheadline4) ) :
              ?>
              <h3 class="text-center headline">
                <?php echo $type5_pt2cardlist1cardheadline4; ?>
              </h3>
              <?php 
                endif ;
              ?>
              <?php 
                if( isset($type5_pt2cardlist1carddesc4) && !empty($type5_pt2cardlist1carddesc4) ) :
              ?>
              <p class="description">
                <?php echo $type5_pt2cardlist1carddesc4; ?>
              </p>
              <?php 
                endif ;
              ?>
            </div>
          </div>
          <div class="col-xs-120 col-sm-24">
            <div class=" resmigrate-card-item border-gray p10 ">
              <?php 
                if( isset($o_type5_pt2cardlist1cardimage5[0]) && !empty($o_type5_pt2cardlist1cardimage5[0]) ) :
              ?>
              <img class="image text-center" src="<?php echo $o_type5_pt2cardlist1cardimage5[0]; ?>" />
              <?php 
                endif;
              ?>
              <?php 
                if( isset($type5_pt2cardlist1cardheadline5) && !empty($type5_pt2cardlist1cardheadline5) ) :
              ?>
              <h3 class="text-center headline">
                <?php echo $type5_pt2cardlist1cardheadline5; ?>
              </h3>
              <?php 
                endif ;
              ?>
              <?php 
                if( isset($type5_pt2cardlist1carddesc5) && !empty($type5_pt2cardlist1carddesc5) ) :
              ?>
              <p class="description">
                <?php echo $type5_pt2cardlist1carddesc5; ?>
              </p>
              <?php 
                endif ;
              ?>
            </div>
          </div>
        </div>


        <div class="row mb50">
          <div class="col-xs-120">

            <?php 
              if( isset($type5_pt2concernlist1headline1) && !empty($type5_pt2concernlist1headline1) ) :
            ?>
            <header class="border-bottom border-sm mb40">
              <h2 class="text-center">
                <?php echo $type5_pt2concernlist1headline1; ?>
              </h2>
            </header>
            <?php 
              endif;
            ?>

            <div class="concernlive-list  bg-e5e5e5 p20">
              <ul class="pl0 li-bullet-none">
                <?php 
                  if( isset($type5_pt2concernlist1concern1) && !empty($type5_pt2concernlist1concern1) ) :
                ?>
                <li>
                  <h3 class="concernlive-list-h">
                    <?php echo $type5_pt2concernlist1concern1; ?>
                  </h3>
                  <p>
                    <?php echo $type5_pt2concernlist1desc1; ?>
                  </p>
                </li>
                <?php 
                  endif;
                ?>
                <?php 
                  if( isset($type5_pt2concernlist1concern2) && !empty($type5_pt2concernlist1concern2) ) :
                ?>
                <li>
                  <h3 class="concernlive-list-h">
                    <?php echo $type5_pt2concernlist1concern2; ?>
                  </h3>
                  <p>
                    <?php echo $type5_pt2concernlist1desc2; ?>
                  </p>
                </li>
                <?php 
                  endif;
                ?>
                <?php 
                  if( isset($type5_pt2concernlist1concern3) && !empty($type5_pt2concernlist1concern3) ) :
                ?>
                <li>
                  <h3 class="concernlive-list-h">
                    <?php echo $type5_pt2concernlist1concern3; ?>
                  </h3>
                  <p>
                    <?php echo $type5_pt2concernlist1desc3; ?>
                  </p>
                </li>
                <?php 
                  endif;
                ?>
                <?php 
                  if( isset($type5_pt2concernlist1concern4) && !empty($type5_pt2concernlist1concern4) ) :
                ?>
                <li>
                  <h3 class="concernlive-list-h">
                    <?php echo $type5_pt2concernlist1concern4; ?>
                  </h3>
                  <p>
                    <?php echo $type5_pt2concernlist1desc4; ?>
                  </p>
                </li>
                <?php 
                  endif;
                ?>
                <?php 
                  if( isset($type5_pt2concernlist1concern5) && !empty($type5_pt2concernlist1concern5) ) :
                ?>
                <li>
                  <h3 class="concernlive-list-h">
                    <?php echo $type5_pt2concernlist1concern5; ?>
                  </h3>
                  <p>
                    <?php echo $type5_pt2concernlist1desc5; ?>
                  </p>
                </li>
                <?php 
                  endif;
                ?>
              </ul>


            </div>

          </div>
        </div>


        <div class="row mb40">
          <div class="col-xs-120">
            <?php 
              if( isset($type5_pt2cardlist2desc1) && !empty($type5_pt2cardlist2desc1) ) :
            ?>
              <h2>
                <?php echo $type5_pt2cardlist2desc1; ?>
              </h2>
            <?php 
              endif;
            ?>
          </div>
        </div>

        <div class="row mb40">
          <div class="col-xs-120 col-sm-60">
            <div class="visatype-card-item border-gray p20">
              <?php 
                if( isset($o_type5_pt2cardlist2cardimage1[0]) && !empty($o_type5_pt2cardlist2cardimage1[0]) ) :
              ?>
              <img class="image text-center" src="<?php echo $o_type5_pt2cardlist2cardimage1[0]; ?>" />
              <?php 
                endif;
              ?>
              <?php 
                if( isset($type5_pt2cardlist2cardheadline1) && !empty($type5_pt2cardlist2cardheadline1) ) :
              ?>
              <h3 class="text-center headline">
                <?php echo $type5_pt2cardlist2cardheadline1; ?>
              </h3>
              <?php 
                endif ;
              ?>
              <?php 
                if( isset($type5_pt2cardlist2carddesc1) && !empty($type5_pt2cardlist2carddesc1) ) :
              ?>
              <p class="description">
                <?php echo $type5_pt2cardlist2carddesc1; ?>
              </p>
              <?php 
                endif ;
              ?>
            </div>
          </div>
          <div class="col-xs-120 col-sm-60">
            <div class="visatype-card-item border-gray p20">
              <?php 
                if( isset($o_type5_pt2cardlist2cardimage2[0]) && !empty($o_type5_pt2cardlist2cardimage2[0]) ) :
              ?>
              <img class="image text-center" src="<?php echo $o_type5_pt2cardlist2cardimage2[0]; ?>" />
              <?php 
                endif;
              ?>
              <?php 
                if( isset($type5_pt2cardlist2cardheadline2) && !empty($type5_pt2cardlist2cardheadline2) ) :
              ?>
              <h3 class="text-center headline">
                <?php echo $type5_pt2cardlist2cardheadline2; ?>
              </h3>
              <?php 
                endif ;
              ?>
              <?php 
                if( isset($type5_pt2cardlist2carddesc2) && !empty($type5_pt2cardlist2carddesc2) ) :
              ?>
              <p class="description">
                <?php echo $type5_pt2cardlist2carddesc2; ?>
              </p>
              <?php 
                endif ;
              ?>
            </div>
          </div>
        </div>



        <div class="row mb40">
          <div class="col-xs-120">

            <?php 
              if( isset($type5_pt2desclist1headline1) && !empty($type5_pt2desclist1headline1) ) :
            ?>
            <header class="border-bottom border-sm mb40">
              <h2 class="text-center">
                <?php echo $type5_pt2desclist1headline1; ?>
              </h2>
            </header>
            <?php 
              endif;
            ?>

            <p>
              ハワイの移住を本格的に実現する際、ビザにも様々な種類があります。
            </p> 


            <div class="visa-list">
              <ul class="pl0 li-bullet-none mb30">
                  <?php 
                    if( isset($type5_pt2desclist1descheadline1) && !empty($type5_pt2desclist1descheadline1) ) :
                  ?>
                  <li>
                    <h3 class="visa-list-h">
                      <?php echo $type5_pt2desclist1descheadline1; ?>
                    </h3>
                    <p>
                      <?php echo $type5_pt2desclist1descdescription1; ?>
                    </p>
                  </li>
                  <?php 
                    endif;
                  ?>
                  <?php 
                    if( isset($type5_pt2desclist1descheadline2) && !empty($type5_pt2desclist1descheadline2) ) :
                  ?>
                  <li>
                    <h3 class="visa-list-h">
                      <?php echo $type5_pt2desclist1descheadline2; ?>
                    </h3>
                    <p>
                      <?php echo $type5_pt2desclist1descdescription2; ?>
                    </p>
                  </li>
                  <?php 
                    endif;
                  ?>
                  <?php 
                    if( isset($type5_pt2desclist1descheadline3) && !empty($type5_pt2desclist1descheadline3) ) :
                  ?>
                  <li>
                    <h3 class="visa-list-h">
                      <?php echo $type5_pt2desclist1descheadline3; ?>
                    </h3>
                    <p>
                      <?php echo $type5_pt2desclist1descdescription3; ?>
                    </p>
                  </li>
                  <?php 
                    endif;
                  ?>
                  <?php 
                    if( isset($type5_pt2desclist1descheadline4) && !empty($type5_pt2desclist1descheadline4) ) :
                  ?>
                  <li>
                    <h3 class="visa-list-h">
                      <?php echo $type5_pt2desclist1descheadline4; ?>
                    </h3>
                    <p>
                      <?php echo $type5_pt2desclist1descdescription4; ?>
                    </p>
                  </li>
                  <?php 
                    endif;
                  ?>
              </ul>

              <?php 
                if( isset($type5_pt2desclist1description1) && !empty($type5_pt2desclist1description1) ) :
              ?>
              <p class="weight-bold">
                <?php echo $type5_pt2desclist1description1; ?>
              </p>
              <?php 
                endif;
              ?>
            </div>

            <h2 class="text-center">ハワイに実際移住する事になったら</h2> 
            <p>ハワイ滞在にはハッキリとした目的を持ち、米国からの滞在許可(ビザ)が降りない限 り移住は出来ません。よって、ハワイでの暮らしを考えた時、企業や留学など何らかの方法を 取るのが一般的です。</p>            

          </div>
        </div>


          

      </div>




    </section>
  <?php 
    /*endif ;*/
  ?>

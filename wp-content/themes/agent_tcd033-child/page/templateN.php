<?php
  if(get_post_meta($post->ID,'page_main_image',true)) {
  if(is_mobile()){
    if(get_post_meta($post->ID, 'page_main_image_mobile', true)){
      $value = get_post_meta($post->ID, 'page_main_image_mobile', true);
    }else{
      $value = get_post_meta($post->ID, 'page_main_image', true);
    }
  }else{
    $value = get_post_meta($post->ID, 'page_main_image', true);
  };
  }else{
  if(has_post_thumbnail()){
    $value = get_post_thumbnail_id($post->ID);
  };
  };
  $image = wp_get_attachment_image_src($value, 'full');
?>


<?php if($value): ?>
<div class="container-fluid">
  <div class="row ">
    <div class="col-xs-120 page-splash hidden-xs" data-parallax="scroll" data-image-src="<?php echo $image[0]; ?>"></div>
    <div class="col-xs-120 visible-xs" style="padding:0; height:300px; background-image:url(<?php echo $image[0]; ?>); background-size:cover; background-repeat: no-repeat; background-position:center center;"></div>
  </div>
</div>
<?php endif; ?> 

<div class="container" style="background: white">
  <div class="row">
    <div class="col-xs-120">
      <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
          <?php 
          if(function_exists('bcn_display'))
          {
              bcn_display();
          }
          ?>
      </div>
    </div>
  </div>
</div>

<?php 
  the_content();
?>